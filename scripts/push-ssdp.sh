#!/usr/bin/env bash
WORKSPACE_ROOT=$(dirname $(realpath ${BASH_SOURCE[0]}))/..

. $WORKSPACE_ROOT/scripts/common/docker-support.sh.inc
. $WORKSPACE_ROOT/scripts/common/call-support.sh.inc
. $WORKSPACE_ROOT/scripts/common/param-support.sh.inc

set -e

function scanner_ssh {
	$(arg ip $@)
	$(arg ssh-key $@)
	$(arg ssh-user $@)
	shift; shift; shift
	ssh -i ${ssh_key} -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no ${ssh_user}@${ip} "$@"
}

function push_ssdp {
	$(optarg ip "$(get_userparam scanner.ip)" $@)
	$(optarg ssh-user "system" $@)
	$(optarg ssh-key "$(get_userparam scanner.user.system.sshKey)" $@)

	pushd $(mktemp -d) >/dev/null
	echo "Packaging..."
	echo "PWD: $PWD -- $WORKSPACE_ROOT/$(get_param repo.scannersoftware.path)/ScanSoftware/ScannerSoftware/SsdpServer"
	tar -czf ssdp.tgz -C $WORKSPACE_ROOT/$(get_param repo.scannersoftware.path)/ScanSoftware/ScannerSoftware/SsdpServer/bundle bundle.js
	echo "Uploading..."
	sftp -q -i "${ssh_key}" -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no ${ssh_user}@${ip} <<< $'cd data/ScanOpD/\nput ssdp.tgz'
	echo "Extracting..."
	scanner_ssh --ssh-key=${ssh_key} --ssh-user=${ssh_user} --ip=${ip} "cd /data/ScanOpD && rm -rf SsdpServer && mkdir SsdpServer && busybox tar xzf ssdp.tgz -C SsdpServer && rm -rf ssdp.tgz"
	rm -rf $PWD
	popd >/dev/null
}

push_ssdp "$@"
