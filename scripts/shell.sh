#!/usr/bin/env bash
PARENT_DIR=$(dirname $(realpath ${BASH_SOURCE[0]}))
WORKSPACE_ROOT=$PARENT_DIR/..

. $PARENT_DIR/common/docker-support.sh.inc
. $PARENT_DIR/common/call-support.sh.inc
. $PARENT_DIR/common/param-support.sh.inc

function scanner_ssh {
	$(arg ip $@);
	$(arg ssh-key $@);
	shift; shift
	echo "___${ssh_key}___"
	echo "___${ip}___"
	echo "___$@___"
	ssh -t -i ${ssh_key} -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@$ip "$@"
}

ssh_shell() {
	$(optarg scanner-ip "$(get_userparam scanner.ip)" $@)
	$(optarg ssh-key "$(get_userparam scanner.user.root.sshKey)" $@)
	echo "IP: ${scanner_ip}"
	cd $WORKSPACE_ROOT
	scanner_ssh --ip=${scanner_ip} --ssh-key=${ssh_key} "$@"
}

ssh_shell $@