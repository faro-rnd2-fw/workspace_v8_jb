#!/usr/bin/env bash
PARENT_DIR=$(dirname $(realpath ${BASH_SOURCE[0]}))
WORKSPACE_ROOT=$PARENT_DIR/..

. $PARENT_DIR/common/docker-support.sh.inc
. $PARENT_DIR/common/call-support.sh.inc
. $PARENT_DIR/common/param-support.sh.inc

set -e

function build_scanopd {
	$(arg docker-image $@)
	$(arg checkout-dir $@)
	$(arg install-prefix $@)
	$(arg with-cpprestsdk $@)
	$(arg with-boost $@)
	$(arg with-openssl $@)
	$(arg with-nodejs $@)
	$(arg with-samba $@)
	$(arg with-lswebserver $@)
	$(arg with-eigen $@)
	$(arg with-nlohmannjson $@)
	$(arg with-androidsdk $@)
	$(arg with-androidndk $@)
	$(arg with-androidndk-standalone $@)
	$(optarg with-ccache "" $@)
	$(optarg config "debug" $@)
	$(optarg njobs 6 $@)
	$(optarg user "root" $@)

	mkdir -p "${install_prefix}"

	if [ -z "${with_ccache}" ]; then
		local CCACHE_ARG=
		local USE_CCACHE=0
	else
		local CCACHE_ARG="-v $(realpath ${with_ccache}):/work/ScanSoftware/.deps/ccache"
		local USE_CCACHE=1
	fi

	local DOCKER_ARGS="${CCACHE_ARG} \
		--name scanopd_build \
		-v $(realpath ${with_nlohmannjson}):/work/ScanSoftware/.deps/nlohmannjson \
		-v $(realpath ${with_androidsdk}):/android/sdk \
		-v $(realpath ${with_androidndk}):/android/ndk-dist \
		-v $(realpath ${with_androidndk_standalone}):/android/ndk-toolchain-gcc \
		-v $(realpath ${with_eigen}):/work/ScanSoftware/.deps/eigen \
		-v $(realpath ${with_lswebserver}):/work/ScanSoftware/.deps/lswebserver \
		-v $(realpath ${with_samba}):/work/ScanSoftware/.deps/samba \
		-v $(realpath ${with_cpprestsdk}):/work/ScanSoftware/.deps/cpprestsdk \
		-v $(realpath ${with_openssl}):/work/ScanSoftware/.deps/openssl \
		-v $(realpath ${with_nodejs}):/work/ScanSoftware/.deps/nodejs \
		-v $(realpath ${with_boost}):/work/ScanSoftware/.deps/boost \
		-v $(realpath ${checkout_dir}):/work \
		-v $(realpath ${install_prefix}):/output \
		--user=${user}"

	docker_run "${docker_image}" "${DOCKER_ARGS}" "$(cat << DOCKER_END
		set -o pipefail -e

		cp /work/ScanSoftware/.deps/samba/* /work/ScanSoftware/ScannerSoftware/SambaServer/Files/
		cp /work/ScanSoftware/.deps/lswebserver/* /work/ScanSoftware/Android/ScanOpD/assets/
		cd /work/ScanSoftware/Android/ScanOpD

		export ANDROID_NDK_ROOT=/android/ndk-dist
		export ANDROID_NDK_STANDALONE=/android/ndk-toolchain-gcc
		export ANDROID_SDK_ROOT=/android/sdk
		export ANDROID_HOME=/android/sdk	
		export ANDROID_BUILD_NJOBS=${njobs}
		export PATH=/work/ScanSoftware/.deps/ccache/bin:\$PATH
		export USE_CCACHE=${USE_CCACHE}
		export CCACHE_DIR=/work/ScanSoftware/.deps/ccache/.ccache
		unset CXXFLAGS

		\$ANDROID_SDK_ROOT/tools/android update project -p .
		mkdir -p ~/.android
		cp -v platform_debug.jks  ~/.android/debug.keystore
		ant ${config}
		if [ "${config}" == "release" ] || [ "${config}" == "release_assert" ]; then
			cp bin/ScanOpD-release-unsigned.apk bin/ScanOpD-release-unaligned.apk
			jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -storepass FARO_android -keypass FARO -keystore platform.jks bin/ScanOpD-release-unaligned.apk FARO
			/android/sdk/tools/zipalign -v -f 4 bin/ScanOpD-release-unaligned.apk bin/ScanOpD-release.apk
			cp -av bin/ScanOpD-release.apk /output/ScanOpD-${config}.apk
		else
			cp -av bin/ScanOpD-${config}.apk /output/
		fi
		echo "done...."
DOCKER_END
)"
}

do_build() {
	$(optarg config "$(get_userparam build.config)" $@)
	$(optarg njobs 16 $@)

	cd ${WORKSPACE_ROOT}
	trap "docker kill scanopd_build >/dev/null 2>&1 || true" EXIT
	
	echo "===== Starting build ====================="
	build_scanopd \
		--njobs=$njobs \
		--config=$config \
		--docker-image=$(get_param toolchain.docker.image) \
		--checkout-dir=$(get_param repo.scannersoftware.path) \
		--install-prefix=$(get_userparam build.artifacts.scanopd.path) \
		--with-cpprestsdk=$(get_param deps.cpprestsdk.path) \
		--with-boost=$(get_param deps.boost.path) \
		--with-openssl=$(get_param deps.openssl.path) \
		--with-nodejs=$(get_param deps.nodejs.path) \
		--with-samba=$(get_param deps.samba.path) \
		--with-lswebserver=$(get_param deps.lswebserver.path) \
		--with-eigen=$(get_param deps.eigen.path) \
		--with-ccache=$(get_param deps.ccache.path) \
		--with-nlohmannjson=$(get_param deps.nlohmannjson.path) \
		--with-androidsdk=$(get_param deps.androidsdk.path) \
		--with-androidndk=$(get_param repo.ndkDist.path) \
		--with-androidndk-standalone=$(get_param toolchain.standalone.path) \
		$@
}

do_build "$@"
