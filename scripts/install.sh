#!/usr/bin/env bash
WORKSPACE_ROOT=$(dirname $(realpath ${BASH_SOURCE[0]}))/..

. $WORKSPACE_ROOT/scripts/common/call-support.sh.inc
. $WORKSPACE_ROOT/scripts/common/param-support.sh.inc

set -e

function scanner_ssh {
	$(arg ip $@)
	$(arg ssh-key $@)
	shift; shift
	ssh -i ${ssh_key} -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@${ip} "$@"
}

function install_scanopd {
	$(arg ip $@)
	$(arg apk $@)
	$(arg ssh-key $@)

	local apk_filename=$(basename ${apk})

	scanner_ssh --ssh-key=${ssh_key} --ip=${ip} "am broadcast -a com.faroeurope.scanopd.terminate"
	sftp -i ${ssh_key} -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@${ip} <<< $'cd data\nput '"${apk}"
	sleep 2
	scanner_ssh --ssh-key=${ssh_key} --ip=${ip} "pm uninstall com.faroeurope.scanopd"
	scanner_ssh --ssh-key=${ssh_key} --ip=${ip} "rm -rf /device/ScanOpD/ScanOpD.txt"
	scanner_ssh --ssh-key=${ssh_key} --ip=${ip} "rm -rf /device/ScanOpD/ScanOpD_*.txt.gz"
	scanner_ssh --ssh-key=${ssh_key} --ip=${ip} "cd data && chmod 777 ${apk_filename} && pm install -r ${apk_filename}"
	scanner_ssh --ssh-key=${ssh_key} --ip=${ip} "am start com.faroeurope.scanopd/.GUI"
}

do_install() {
	$(optarg scanner-ip "$(get_userparam scanner.ip)" $@)
	$(optarg config "$(get_userparam build.config)" $@)
	$(optarg ssh-key "$(get_userparam scanner.user.root.sshKey)" $@)
	$(optarg apk "$(get_userparam build.artifacts.scanopd.path)/ScanOpD-$(get_userparam build.config).apk" $@)
	echo "Installing ${apk} on scanner at ${scanner_ip}"
	pushd "${WORKSPACE_ROOT}" >/dev/null
	safely install_scanopd \
		--ip=${scanner_ip} \
		--ssh-key=${ssh_key} \
		--apk=${apk}
	popd >/dev/null
}

do_install "$@"

