#!/usr/bin/env bash
WORKSPACE_ROOT=$(dirname $(realpath ${BASH_SOURCE[0]}))/..

. $WORKSPACE_ROOT/scripts/common/docker-support.sh.inc
. $WORKSPACE_ROOT/scripts/common/call-support.sh.inc
. $WORKSPACE_ROOT/scripts/common/param-support.sh.inc


# Builds the V8 LsWebserver component
# @param --with-node=<PATH>         The path to a folder containing the node-executable for Android
# @param --scansoftware-checkout-dir=<PATH> The location of the scansoftware checkout-dir
# @param --checkout-dir=<PATH>      The location of the sources from which to build
# @param --install_prefix=<PATH>    The location where to place the build artifacts
function build_lswebserver {
	$(arg docker-image $@)
	$(arg checkout-dir $@)
	$(arg scansoftware-checkout-dir $@)	
	$(arg install-prefix $@)
	$(arg with-node $@)
	$(arg config $@)

	mkdir -p "${install_prefix}"

	local DOCKER_ARGS="-v $(realpath ${checkout_dir}):/work -v $(realpath ${install_prefix}):/output -v $(realpath ${with_node}):/deps/node -v $(realpath ${scansoftware_checkout_dir}):/deps/scansoftware"
	TIME=`date`

	docker_run "${docker_image}" "${DOCKER_ARGS}" "$(cat << EOF
		set -o pipefail -e

		cd /work
		./createLSWebServerPackage.sh ${config} /deps/scansoftware/HtmlGui2 /output /deps/node/node
EOF
)"
}

do_build() {
	$(optarg config "$(get_userparam build.config)" $@)
	cd $WORKSPACE_ROOT

	ls -al "$(get_param deps.nodejs.path)"

	build_lswebserver \
		--config=$config \
		--docker-image=$(get_param toolchain.docker.lswebserver_image) \
		--checkout-dir=$(get_param repo.lswebserver.path) \
		--scansoftware-checkout-dir=$(get_param repo.scannersoftware.path) \
		--with-node=$(get_param deps.nodejs.path) \
		--install-prefix=$(get_param deps.lswebserver.path)
}

do_build "$@"