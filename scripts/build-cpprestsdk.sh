#!/usr/bin/env bash
PARENT_DIR=$(dirname $(realpath ${BASH_SOURCE[0]}))
WORKSPACE_ROOT=$PARENT_DIR/..

. $PARENT_DIR/common/docker-support.sh.inc
. $PARENT_DIR/common/call-support.sh.inc
. $PARENT_DIR/common/param-support.sh.inc

set -e -o pipefail -u

function build_cpprestsdk {
	$(arg checkout-dir $@)
	$(arg install-prefix $@)
	$(arg docker-image $@)
	$(arg with-boost $@)
	$(arg with-openssl $@)
	$(arg with-openssl $@)
	$(arg with-android-cmake $@)
	$(arg with-androidsdk $@)
	$(arg with-androidndk $@)
	$(arg with-androidndk-standalone $@)

	echo "==${with_android_cmake}"

	mkdir -p "${install_prefix}"

	local DOCKER_ARGS="-v $(realpath ${checkout_dir}):/work \
		-v $(realpath ${with_android_cmake}):/android-cmake \
		-v $(realpath ${with_androidsdk}):/android/sdk \
		-v $(realpath ${with_androidndk}):/android/ndk-dist \
		-v $(realpath ${with_androidndk_standalone}):/android/ndk-toolchain-gcc \
		-v $(realpath ${install_prefix}):/output \
		-v $(realpath ${with_boost}):/boost \
		-v $(realpath ${with_openssl}):/openssl"

	docker_run "${docker_image}" "${DOCKER_ARGS}" "$(cat << 'DOCKER_END'
cd /work
set -o pipefail -e
echo "=====${NDK_ROOT}"
export NDK_ROOT=/android/ndk-dist
echo "r10e">${NDK_ROOT}/RELEASE.TXT
mkdir -p build && cd build
make --version
cmake ../Release/ -DCMAKE_MAKE_PROGRAM=make -DBUILD_SHARED_LIBS=OFF -DCMAKE_TOOLCHAIN_FILE=/android-cmake/android.toolchain.cmake -DANDROID_TOOLCHAIN_NAME=arm-linux-androideabi-5 -DANDROID_STL=none -DANDROID_STL_FORCE_FEATURES=ON -DANDROID_NATIVE_API_LEVEL=android-17 -DANDROID_GOLD_LINKER=OFF -DCMAKE_BUILD_TYPE=Release -DANDROID_NDK=/android/ndk-dist -DBOOST_INCLUDEDIR=/boost/include -DBOOST_LIBRARYDIR=/boost/lib -DCMAKE_CXX_FLAGS='-I/android/ndk-toolchain-gcc/include/c++/5.3 -I/android/ndk-toolchain-gcc/include/c++/5.3/arm-linux-androideabi -I/openssl/include -DSIZE_MAX="((size_t)-1)"'

cmake --build . --config Release -- -j6
mkdir -p /output/lib
cp -a Binaries/*.a /output/lib
rm Binaries/*.a
cp -a Binaries /output/bin
cp -a ../Release/include /output/include
DOCKER_END
)"
}

do_build() {
	cd ${WORKSPACE_ROOT}
	trap "docker kill scanopd_build >/dev/null 2>&1 || true" EXIT
	
	build_cpprestsdk \
		--docker-image=$(get_param toolchain.docker.image) \
		--checkout-dir=$(get_param repo.cpprestsdk.path) \
		--install-prefix=$(get_param deps.cpprestsdk.path) \
		--with-boost=$(get_param deps.boost.path) \
		--with-openssl=$(get_param deps.openssl.path) \
		--with-android-cmake=$(get_param repo.androidcmake.path) \
		--with-androidsdk=$(get_param deps.androidsdk.path) \
		--with-androidndk=$(get_param repo.ndkDist.path) \
		--with-androidndk-standalone=$(get_param toolchain.standalone.path) \
		$@
}

do_build "$@"
