#!/usr/bin/env bash
PARENT_DIR=$(dirname $(realpath ${BASH_SOURCE[0]}))
WORKSPACE_ROOT=$PARENT_DIR/..

. $PARENT_DIR/common/docker-support.sh.inc
. $PARENT_DIR/common/call-support.sh.inc
. $PARENT_DIR/common/param-support.sh.inc

set -e

function compile {
	$(arg docker-image $@)
	$(arg checkout-dir $@)
	$(arg with-cpprestsdk $@)
	$(arg with-boost $@)
	$(arg with-openssl $@)
	$(arg with-nodejs $@)
	$(arg with-samba $@)
	$(arg with-lswebserver $@)
	$(arg with-eigen $@)
	$(arg with-nlohmannjson $@)
	$(arg with-androidsdk $@)
	$(arg with-androidndk $@)
	$(arg with-androidndk-standalone $@)
	$(arg file $@)
	$(optarg with-ccache "" $@)
	$(optarg config "debug" $@)
	$(optarg user "root" $@)

	if [ -z "${with_ccache}" ]; then
		local CCACHE_ARG=
		local USE_CCACHE=0
	else
		local CCACHE_ARG="-v $(realpath ${with_ccache}):/work/ScanSoftware/.deps/ccache"
		local USE_CCACHE=1
	fi


	local CXX_FLAGS="-o /tmp/.delete-me -w -x c++ -c -DBOOST_MPL_CFG_NO_PREPROCESSED_HEADERS -DBOOST_MPL_LIMIT_LIST_SIZE=30 -Wsign-compare -Wmaybe-uninitialized -Werror=maybe-uninitialized -Werror=return-type -fno-diagnostics-color -D_UNICODE -D_LINUX -DANDROID -std=c++14 -Wall -Wclobbered -Wtype-limits -Winvalid-pch -Werror=return-type -Wno-deprecated-declarations -Werror=format -Werror=format-extra-args -Werror=write-strings -fno-strict-aliasing -Wno-reorder -Wno-comment -Wno-unknown-pragmas -Wno-unused-variable -Werror=delete-non-virtual-dtor -Werror=missing-braces -Werror=missing-field-initializers -Werror=enum-compare -mcpu=cortex-a9 -mtune=cortex-a9 -mthumb -pipe"

	local INCLUDES="-I/work/ScanSoftware -I/work/Scanner/V6/LsCommon/include -I/work/ScanSoftware/Common -I/work/ScanSoftware/Public_Libs -I/work/ScanSoftware/Extern/breakpad/src -I/work/ScanSoftware/Extern/breakpad/src/common/android/include -I/work/ScanSoftware/Extern/uuid-1.6.2 -I/work/ScanSoftware/Extern/libgsf-1.14.4 -I/work/ScanSoftware/Extern/glib -I/work/ScanSoftware/Extern/glib/android -I/work/ScanSoftware/Extern/glib/glib -I/work/ScanSoftware/Extern/glib/gobject/android -I/work/ScanSoftware/Extern -I/work/ScanSoftware/Extern/xml/xerces/src -I/work/ScanSoftware/Extern/PaintLib/PAINTLIB/common -I/work/ScanSoftware/Extern/PaintLib/OTHERLIB/libpng -I/work/ScanSoftware/Extern/PaintLib/OTHERLIB/libtiff -I/work/ScanSoftware/Extern/PaintLib/OTHERLIB/libjpeg-android -I/work/ScanSoftware/Extern/PaintLib/OTHERLIB/libungif -I/work/ScanSoftware/Extern/OpenCV2/include -I/work/ScanSoftware/Extern/OpenCV2/modules/core/include -I/work/ScanSoftware/Extern/OpenCV2/modules/flann/include -I/work/ScanSoftware/Extern/OpenCV2/modules/imgproc/include -I/work/ScanSoftware/Extern/OpenCV2/modules/photo/include -I/work/ScanSoftware/Extern/OpenCV2/modules/video/include -I/work/ScanSoftware/Extern/OpenCV2/modules/features2d/include -I/work/ScanSoftware/Extern/OpenCV2/modules/objdetect/include -I/work/ScanSoftware/Extern/OpenCV2/modules/calib3d/include -I/work/ScanSoftware/Extern/OpenCV2/modules/ml/include -I/work/ScanSoftware/Extern/OpenCV2/modules/highgui/include -I/work/ScanSoftware/Extern/OpenCV2/modules/contrib/include -I/work/ScanSoftware/Extern/OpenCV2/modules/legacy/include -I/work/ScanSoftware/Extern/zlib -I/work/ScanSoftware/Extern/tbb/include -I/work/ScanSoftware/Extern/stxxl/include -I/work/ScanSoftware/Extern/stxxl/src/include -I/work/ScanSoftware/Extern/pc2/pc2_renderer/include/pc2 -I/work/ScanSoftware/Extern/pc2/pc2_converter/include/pc2 -I/work/ScanSoftware -I/work/ScanSoftware/Extern/libiconv-1.13.1/include -I/work/ScanSoftware/Extern/LinuxOnly -I/work/ScanSoftware/Extern/Cryptopp -I/work/ScanSoftware/.deps/eigen/include -I/work/ScanSoftware/Public_Libs -I/work/ScanSoftware/Common -I/work/ScanSoftware/Common/Core -I/work/ScanSoftware/Common/iQLib -I/work/ScanSoftware/Common/iQScanCtrl -I/work/ScanSoftware/ScannerSoftware  -I/work/ScanSoftware/ScannerSoftware/iQScanCtrl -I/work/ScanSoftware/ScannerSoftware/ScanOpD -I/work/ScanSoftware/Extern -I/work/ScanSoftware/Extern/uuid-1.6.2 -I/work/ScanSoftware/Extern/breakpad/src -I/work/ScanSoftware/Extern/uuid -I/work/ScanSoftware/Extern/libgsf-1.14.4 -I/work/ScanSoftware/Extern/breakpad/src -I/work/ScanSoftware/Extern/xml/xerces/src -I/work/ScanSoftware/Extern/tbb/include -I/work/ScanSoftware/Extern/OpenCV2/include -I/work/ScanSoftware/Extern/OpenCV2/modules/core/include -I/work/ScanSoftware/Extern/OpenCV2/modules/imgproc/include -I/work/ScanSoftware/Extern/OpenCV2/modules/calib3d/include -I/work/ScanSoftware/Extern/OpenCV2/modules/contrib/include -I/work/ScanSoftware/Extern/OpenCV2/modules/features2d/include -I/work/ScanSoftware/Extern/OpenCV2/modules/flann/include -I/work/ScanSoftware/Extern/OpenCV2/modules/highgui/include -I/work/ScanSoftware/Extern/OpenCV2/modules/ml/include -I/work/ScanSoftware/Extern/OpenCV2/modules/objdetect/include -I/work/ScanSoftware/Extern/OpenCV2/modules/photo/include -I/work/ScanSoftware/Extern/OpenCV2/modules/video/include -I/work/ScanSoftware/Extern/OpenCV2/modules/legacy/include -I/work/ScanSoftware/Extern/PaintLib/PAINTLIB/common -I/work/ScanSoftware/.deps/cpprestsdk/include -I/work/ScanSoftware/.deps/boost/include   -I/work/ScanSoftware/.deps/nlohmannjson/include -I/work/ScanSoftware/.deps/openssl/include -I/work/ScanSoftware/ScannerSoftware/ScannerWebServer/include -I/work/ScanSoftware/ScannerSoftware/PanoCam/include -I/work/ScanSoftware/Common/Logging/include  -I/work/ScanSoftware/ScannerSoftware/Services/include -I/work/ScanSoftware/Common -I/work/ScanSoftware/Extern/CppUnit/include -I/work/ScanSoftware/FAROScene -I/work/ScanSoftware/ScannerSoftware/LsWebApi/include  -I/work/ScanSoftware/ScannerSoftware/ScanLocalizerAccess/include  -I/work/ScanSoftware/ScannerSoftware/ScannerConfiguration/include  -I/work/ScanSoftware/ScannerSoftware/ScanPlanCtrl/include  -I/work/ScanSoftware/ScannerSoftware/RosBridge/include -I/work/ScanSoftware/.deps/rangev3/include -I/work/ScanSoftware/.deps/gsl/include -I/work/ScanSoftware/Extern/libmtp/src -I/work/ScanSoftware/ScannerSoftware/ScanOpD/build/v8/Debug/Uuid_Project-prefix/src/Uuid_Project-build -I/work/ScanSoftware/Extern/gsl/include"


	local DOCKER_ARGS="${CCACHE_ARG} \
		-v $(realpath ${with_nlohmannjson}):/work/ScanSoftware/.deps/nlohmannjson \
		-v $(realpath ${with_androidsdk}):/android/sdk \
		-v $(realpath ${with_androidndk}):/android/ndk-dist \
		-v $(realpath ${with_androidndk_standalone}):/android/ndk-toolchain-gcc \
		-v $(realpath ${with_eigen}):/work/ScanSoftware/.deps/eigen \
		-v $(realpath ${with_lswebserver}):/work/ScanSoftware/.deps/lswebserver \
		-v $(realpath ${with_samba}):/work/ScanSoftware/.deps/samba \
		-v $(realpath ${with_cpprestsdk}):/work/ScanSoftware/.deps/cpprestsdk \
		-v $(realpath ${with_openssl}):/work/ScanSoftware/.deps/openssl \
		-v $(realpath ${with_nodejs}):/work/ScanSoftware/.deps/nodejs \
		-v $(realpath ${with_boost}):/work/ScanSoftware/.deps/boost \
		-v $(realpath ${checkout_dir}):/work \
		--user=${user}"

	docker_run "${docker_image}" "${DOCKER_ARGS}" "$(cat << DOCKER_END
		set -o pipefail -e
		cd /work

		export ANDROID_NDK_ROOT=/android/ndk-dist
		export ANDROID_NDK_STANDALONE=/android/ndk-toolchain-gcc
		export ANDROID_SDK_ROOT=/android/sdk
		export ANDROID_HOME=/android/sdk
		export PATH=/work/ScanSoftware/.deps/ccache/bin:\$PATH
		unset CXXFLAGS

		CC=\${ANDROID_NDK_STANDALONE}/bin/arm-linux-androideabi-gcc
		\${CC} ${CXX_FLAGS} ${INCLUDES} ${file}
DOCKER_END
)"
}

do_compile() {
	$(optarg config "$(get_userparam build.config)" $@)

	pushd ${WORKSPACE_ROOT} >/dev/null
	trap "popd >/dev/null" EXIT

	safely compile \
		--config=$config \
		--docker-image=$(get_param toolchain.docker.image) \
		--checkout-dir=$(get_param repo.scannersoftware.path) \
		--with-cpprestsdk=$(get_param deps.cpprestsdk.path) \
		--with-boost=$(get_param deps.boost.path) \
		--with-openssl=$(get_param deps.openssl.path) \
		--with-nodejs=$(get_param deps.nodejs.path) \
		--with-samba=$(get_param deps.samba.path) \
		--with-lswebserver=$(get_param deps.lswebserver.path) \
		--with-eigen=$(get_param deps.eigen.path) \
		--with-ccache=$(get_param deps.ccache.path) \
		--with-nlohmannjson=$(get_param deps.nlohmannjson.path) \
		--with-androidsdk=$(get_param deps.androidsdk.path) \
		--with-androidndk=$(get_param repo.ndkDist.path) \
		--with-androidndk-standalone=$(get_param toolchain.standalone.path) \
		$@
}

do_compile --file=$1
