#!/usr/bin/env bash
WORKSPACE_ROOT=$(dirname $(realpath ${BASH_SOURCE[0]}))/..
. $WORKSPACE_ROOT/scripts/common/call-support.sh.inc
. $WORKSPACE_ROOT/scripts/common/param-support.sh.inc

set -e -o pipefail -u

main() {
	$(optarg config "$(get_userparam build.config)" $@)

	mkdir -p ${WORKSPACE_ROOT}/dump_analysis/symbols

	if [[ "$(get_userparam build.config)" = "debug" ]]; then
		BUILD_DIR=$WORKSPACE_ROOT/$(get_param repo.scannersoftware.path)/ScanSoftware/ScannerSoftware/ScanOpD/build/v8/Debug
	else
		BUILD_DIR=$WORKSPACE_ROOT/$(get_param repo.scannersoftware.path)/ScanSoftware/ScannerSoftware/ScanOpD/build/v8/Release
	fi

	echo "BUILD_DIR=$BUILD_DIR"


	docker run \
		-u $(id -u):$(id -g) \
		-v $WORKSPACE_ROOT/$(get_param deps.breakpad.path)/tools:/breakpad \
		-v $WORKSPACE_ROOT/dump_analysis/symbols:/symbols \
		-v ${BUILD_DIR}:/work \
		--rm -t ubuntu:18.04 \
		/bin/bash -c "$(cat <<'DOCKER_END'
		DUMP_SYMS=/breakpad/dump_syms
		mkdir -p /symbols
		chmod a+x $DUMP_SYMS

		cd /work/
		# generate .so.sym file for each .so file
		for f in $(find -name '*.so'); do
			SYMFILE="$(basename "$f")".sym
			$DUMP_SYMS "$f" > $SYMFILE
		 	HASH=`head -1 $SYMFILE | sed -r 's/[^\ ]+ [^\ ]+ [^\ ]+ ([^\ ]+) [^\ ]+/\1/'`
		 	DIRNAME=/symbols/$(basename $f)/${HASH}
		 	TARGET=${DIRNAME}/$SYMFILE
		 	echo HASH=${HASH} DIRNAME=${DIRNAME} TARGET=${TARGET}
		 	mkdir -p $DIRNAME
			mv $SYMFILE $TARGET
		done

DOCKER_END
)"
}

main $@