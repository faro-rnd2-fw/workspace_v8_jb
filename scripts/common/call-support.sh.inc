#!/bin/bash

# Facility to easily support optional named arguments.
# It returns a string which when evaluated creates a local corresponding to the argument value (or the DEFAULT)
# @param ARGNAME	The name of the argument. E.g. if ARGNAME is "src", then the value can be provided as "--src=SOME_VALUE"
#			If "ARGNAME" contains a dash "-" it will be replaced by an underscore "_" in the resulting local.
#			E.g. "checkout-dir" => "checkout_dir"
# @param DEFAULT	The default value to be used if the argument is missing
# @param ...		All arguments that have been passed to the calling function
function optarg {
	local ARGNAME=$1; shift
	local DEFAULT=$1; shift
	for i in "$@"; do
		case $i in
		--${ARGNAME}=*)
			echo "local ${ARGNAME//-/_}=${i#*=}"
			return 0
			;;
		esac
	done
	echo "local ${ARGNAME//-/_}=${DEFAULT}"
}

# Same as `optarg` but without DEFAULT. If the argument is missing an error message will be printed and the return value is -1.
function arg {
	local ARGNAME=$1; shift
	for i in "$@"; do
		case $i in
		--${ARGNAME}=*)
			echo "local ${ARGNAME//-/_}=${i#*=}"
			return 0
			;;
		esac
	done
	>&2 echo "Argument \"${ARGNAME}\" is missing!"
	return -1
}

# Example usage of `optarg` above:

# function doStuff {
#         $(optarg src "" $@)
#         $(optarg dst "/dev/null" $@)
#         echo "got: src=${src}, dst=${dst}, X"
# }

# doStuff --src=some/folder
# doStuff --dst=output-file --src=input


ensure_available() {
	command -v $1 >/dev/null 2>&1 || { echo "Please ensure that $1 is on your \$PATH" >&2; exit 1; }
}

function safely() {
	local OLDSTATE="$(shopt -po xtrace)"
	function showcallstack(){ docker kill build_v8_android >/dev/null 2>&1 || true; while caller $((n++)); do :; done; }
	set -o pipefail -e
	trap showcallstack EXIT INT TERM
	eval "$*"
	trap - EXIT INT TERM
	eval "${OLDSTATE}"
}

