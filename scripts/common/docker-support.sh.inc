#!/usr/bin/env bash

set -e

# Checks if there is a docker image available with the specified name.
# This assumes the docker executable is available in the $PATH
# @param $1 The name of the image to check for
# @returns true if the specified image is found
# @example:
# 	if ! `exists_docker_image hello-world`; then
# 		docker pull hello-world
# 	fi
function exists_docker_image {
	[[ "$(docker images -q $1 2> /dev/null)" != "" ]]
}

function docker_run {
	local DOCKER_IMAGE=$1
	local ARGS=$2
	local SCRIPT=$3
	#buildDockerImage
	if ! `exists_docker_image ${DOCKER_IMAGE}`; then
		docker pull ${DOCKER_IMAGE}
	fi
	# echo "COMMANDLINE: docker run ${ARGS} -u $(id -g):$(id -u) --rm -t ${DOCKER_IMAGE} /bin/bash -c \"$SCRIPT\""
	# docker run ${ARGS} -u $(id -g):$(id -u) --rm -t ${DOCKER_IMAGE} /bin/bash -c "$SCRIPT"
	docker run ${ARGS} -u $(id -g):$(id -u) --rm -t ${DOCKER_IMAGE} /bin/bash -c "$SCRIPT"
}