#!/bin/bash
PARENT_DIR=$(dirname $(realpath ${BASH_SOURCE[0]}))
WORKSPACE_ROOT=$(dirname $(realpath ${BASH_SOURCE[0]}))/../..

. $PARENT_DIR/call-support.sh.inc

get_param() {
	ensure_available jq
	cat $WORKSPACE_ROOT/config.json | jq -r ".$@"
}

get_userparam() {
	ensure_available jq
	cat $WORKSPACE_ROOT/userconfig.json | jq -r ".$@"
}

