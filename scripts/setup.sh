#!/usr/bin/env bash
PARENT_DIR=$(dirname $(realpath ${BASH_SOURCE[0]}))
WORKSPACE_ROOT=$PARENT_DIR/..

. $PARENT_DIR/common/docker-support.sh.inc
. $PARENT_DIR/common/call-support.sh.inc
. $PARENT_DIR/common/param-support.sh.inc

set -e -o pipefail -u

checkout_git() {
	$(arg repo $@)
	$(optarg checkout-dir "" $@)
	$(optarg branch "" $@)
	ensure_available git

	echo "Setup ${repo} into ${checkout_dir}"
	if [[ -d "${checkout_dir}" ]]; then
		return
	fi

	echo "Cloning ${repo} into ${checkout_dir}"
	if [[ "${branch}" == "" ]]; then
		git clone --recursive "${repo}" "${checkout_dir}"
	else
		git clone --branch ${branch} --recursive "${repo}" "${checkout_dir}"
	fi
}

generate_ndk_standalone() {
	$(arg ndk-checkout-dir $@)
	$(arg install-dir $@)
	local abs_install_dir=$(realpath ${install_dir})

	if [[ -d "${install_dir}" ]]; then
		return
	fi

	pushd "${ndk_checkout_dir}" >/dev/null

	local DOCKER_ARGS="-v $PWD:/work -v $abs_install_dir:/android/ndk-toolchain-gcc -w /work"

	docker_run "$(get_param toolchain.docker.image)" "${DOCKER_ARGS}" "$(cat << DOCKER_END
		build/tools/make-standalone-toolchain.sh \
			--toolchain=arm-linux-androideabi-5 \
			--system=linux-x86_64 \
			--platform=android-17 \
			--install-dir=/android/ndk-toolchain-gcc
DOCKER_END
)"
	popd >/dev/null
}

function teamcity_download {
	$(optarg http-user $@)
	$(optarg http-password $@)
	$(arg url $@)
	wget --auth-no-challenge --http-user=${http_user} --http-password="${http_password}" -qO- "${url}"
}

function download_tar {
	$(arg output-dir $@)
	$(optarg http-user "" $@)
	$(optarg http-password "" $@)
	$(arg url $@)
	echo "Download ${url} into ${output_dir}"
	if [[ -d "${output_dir}" ]]; then
		return
	fi
	mkdir -p "${output_dir}"
	teamcity_download --http-user=${http_user} --http-password=${http_password} --url="${url}" | tar x -C "${output_dir}"
}

function download_zip {
	$(arg output-dir $@)
	$(arg http-user $@)
	$(arg http-password $@)
	$(arg url $@)
	echo "Download ${url} into ${output_dir}"
	if [[ -d "${output_dir}" ]]; then
		echo "already found"
		return
	fi
	mkdir -p "${output_dir}"
	wget --auth-no-challenge --http-user=${http_user} --http-password="${http_password}" -O "__tmp_artifacts.zip" "${url}"
	unzip __tmp_artifacts.zip -d "${output_dir}"
	rm -f __tmp_artifacts.zip
}

function download_samba {
	$(arg output-dir $@)
	$(arg http-user $@)
	$(arg http-password $@)
	$(arg url $@)
	local abs_target_dir=$(realpath ${output_dir} )

	if [ -d "${abs_target_dir}" ]; then
		return
	fi

	local tmpdir=$(mktemp -d -u)

	download_zip \
		--http-user=${http_user} \
		--http-password=${http_password} \
		--url=${url} \
		--output-dir=${tmpdir}

	mv "${tmpdir}/Samba" "${abs_target_dir}"
}


function download_androidsdk {
	$(arg output-dir $@)
	$(arg http-user $@)
	$(arg http-password $@)
	$(arg url $@)
	local abs_target_dir=$(realpath ${output_dir} )

	if [ -d "${abs_target_dir}" ]; then
		return
	fi

	local tmpdir=$(mktemp -d)

	download_zip \
		--http-user=${http_user} \
		--http-password=${http_password} \
		--url=${url} \
		--output-dir=${tmpdir}/artifacts

	unzip ${tmpdir}/artifacts/sdk/linux-x86/android-sdk*.zip -d ${tmpdir}/unzipped
	mv ${tmpdir}/unzipped/android-sdk* "${abs_target_dir}"
	rm -rf ${tmpdir}
}

query_credentials() {
	if [[ -f ${WORKSPACE_ROOT}/userconfig.json ]]; then
		return
	fi
	read -p "TeamCity Username: " teamcity_username
	read -s -p "TeamCity Password: " teamcity_password
	echo
	read -e -p "Scanner IP: " -i "192.168.0.197" scanner_ip
	read -e -p "Scanner SSH root key: " -i "~/.ssh/root_ssh_host_rsa_key" root_ssh_host_rsa_key
	read -e -p "Scanner SSH system key: " -i "~/.ssh/system_ssh_host_rsa_key" root_ssh_host_rsa_key
	read -e -p "Path where to store the ScanOpD artifacts: " -i "artifacts/scanopd" scanopd_artifacts

	cat << EOF >${WORKSPACE_ROOT}/userconfig.json
{
	"build": {
		"config": "debug",
		"artifacts": {
			"scanopd": {
				"path": "${scanopd_artifacts}"
			}
		}
	},
	"scanner": {
		"ip": "${scanner_ip}",
		"user": {
			"root": {
				"sshKey": "${root_ssh_host_rsa_key}"
			},
			"system": {
				"sshKey": "${system_ssh_host_rsa_key}"
			}
		}
	},
	"teamcity": {
		"user": "${teamcity_username}",
		"password": "${teamcity_password}"
	}
}
EOF
}


main() {
	pushd $WORKSPACE_ROOT >/dev/null
	query_credentials

	checkout_git \
		--repo=$(get_param repo.scannersoftware.url) \
		--checkout-dir=$(get_param repo.scannersoftware.path)

	checkout_git \
		--repo=$(get_param repo.lswebserver.url) \
		--checkout-dir=$(get_param repo.lswebserver.path)

	checkout_git \
		--repo=$(get_param repo.cpprestsdk.url) \
		--checkout-dir=$(get_param repo.cpprestsdk.path)

	checkout_git \
		--repo=$(get_param repo.androidcmake.url) \
		--checkout-dir=$(get_param repo.androidcmake.path)

	checkout_git \
		--repo=$(get_param repo.ndkDist.url) \
		--checkout-dir=$(get_param repo.ndkDist.path)

	generate_ndk_standalone \
		--ndk-checkout-dir=$(get_param repo.ndkDist.path) \
		--install-dir=$(get_param toolchain.standalone.path)

	# CppRestSDK
	download_tar \
		--http-user=$(get_userparam teamcity.user) \
		--http-password=$(get_userparam teamcity.password) \
		--url=$(get_param deps.cpprestsdk.url) \
		--output-dir=$(get_param deps.cpprestsdk.path)

	# Eigen
	download_tar \
		--http-user=$(get_userparam teamcity.user) \
		--http-password=$(get_userparam teamcity.password) \
		--url=$(get_param deps.eigen.url) \
		--output-dir=$(get_param deps.eigen.path)

	# OpenSSL
	download_tar \
		--http-user=$(get_userparam teamcity.user) \
		--http-password=$(get_userparam teamcity.password) \
		--url=$(get_param deps.openssl.url) \
		--output-dir=$(get_param deps.openssl.path)

	# NLohmann JSON
	download_tar \
		--http-user=$(get_userparam teamcity.user) \
		--http-password=$(get_userparam teamcity.password) \
		--url=$(get_param deps.nlohmannjson.url) \
		--output-dir=$(get_param deps.nlohmannjson.path)

	# nodejs
	download_zip \
		--http-user=$(get_userparam teamcity.user) \
		--http-password=$(get_userparam teamcity.password) \
		--url=$(get_param deps.nodejs.url) \
		--output-dir=$(get_param deps.nodejs.path)
	chmod a+x $(get_param deps.nodejs.path)/node

	# Boost
	download_tar \
		--http-user=$(get_userparam teamcity.user) \
		--http-password=$(get_userparam teamcity.password) \
		--url=$(get_param deps.boost.url) \
		--output-dir=$(get_param deps.boost.path)

	# ccache
	download_tar \
		--http-user=$(get_userparam teamcity.user) \
		--http-password=$(get_userparam teamcity.password) \
		--url=$(get_param deps.ccache.url) \
		--output-dir=$(get_param deps.ccache.path)

	# Samba
	download_samba \
		--http-user=$(get_userparam teamcity.user) \
		--http-password=$(get_userparam teamcity.password) \
		--url=$(get_param deps.samba.url) \
		--output-dir=$(get_param deps.samba.path)

	# Android SDK
	download_androidsdk \
		--http-user=$(get_userparam teamcity.user) \
		--http-password=$(get_userparam teamcity.password) \
		--url=$(get_param deps.androidsdk.url) \
		--output-dir=$(get_param deps.androidsdk.path)

	# LSWebServer
	download_zip \
		--http-user=$(get_userparam teamcity.user) \
		--http-password=$(get_userparam teamcity.password) \
		--url=$(get_param deps.lswebserver.url) \
		--output-dir=$(get_param deps.lswebserver.path)


	# breakpad
	download_zip \
		--http-user=$(get_userparam teamcity.user) \
		--http-password=$(get_userparam teamcity.password) \
		--url=$(get_param deps.breakpad.url) \
		--output-dir=$(get_param deps.breakpad.path)

	popd >/dev/null
}


main
