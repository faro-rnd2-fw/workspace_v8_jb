#!/usr/bin/env bash
WORKSPACE_ROOT=$(dirname $(realpath ${BASH_SOURCE[0]}))/..
. $WORKSPACE_ROOT/scripts/common/call-support.sh.inc
. $WORKSPACE_ROOT/scripts/common/param-support.sh.inc

set -e -o pipefail -u

echo  $WORKSPACE_ROOT/dump_analysis/dumps/
mkdir -p $WORKSPACE_ROOT/dump_analysis/dumps/

analyze_dumps() {
	$(optarg scanner-ip "$(get_userparam scanner.ip)" $@)
	$(optarg ssh-user "root" $@)		
	$(optarg ssh-key "$(get_userparam scanner.user.root.sshKey)" $@)
	$(optarg local "false" $@)
	cd $WORKSPACE_ROOT
	ssh_key="${ssh_key/#\~/$HOME}"
	local keyname="$(basename ${ssh_key})"
	local keydir="$(dirname ${ssh_key})"
	echo "${ssh_key} ::: ${keyname} ::: ${keydir}"

	if [[ "${local}" == "false" ]]; then
		rm -rf $WORKSPACE_ROOT/dump_analysis/dumps/*.dmp
		rm -rf $WORKSPACE_ROOT/dump_analysis/dumps/*.dmp.err
		docker run -u $(id -u):$(id -g) \
			-v $WORKSPACE_ROOT/$(get_param deps.breakpad.path):/breakpad \
			-v $WORKSPACE_ROOT/dump_analysis/symbols:/symbols \
			-v $WORKSPACE_ROOT/dump_analysis/dumps:/dumps \
			-v ${keydir}:/.ssh \
			--rm -t krmvrsdokr01.faroeurope.com/lsv8/android-ndk-build \
			/bin/bash -c "$(cat <<DOCKER_END
				echo "Fetching dumps from scanner"
				cd /dumps
				sftp -i /.ssh/${keyname} -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@$scanner_ip <<< \$'cd /device/ScanOpD/\nget *.dmp'
				sftp -i /.ssh/${keyname} -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@$scanner_ip <<< \$'cd /device/ScanOpD/\nget minidumps.log'
DOCKER_END
)"
	fi

	docker run \
		-v $WORKSPACE_ROOT/$(get_param deps.breakpad.path):/breakpad \
		-v $WORKSPACE_ROOT/dump_analysis/symbols:/symbols \
		-v $WORKSPACE_ROOT/dump_analysis/dumps:/dumps \
		--rm -t ubuntu:16.04 \
		/bin/bash -c "$(cat <<'DOCKER_END'
			set -e -o pipefail -u
			echo "analyzing dumps in /dumps/ ..."
			chmod a+x /breakpad/tools/minidump_stackwalk
			for i in `ls /dumps/*.dmp`; do
				echo "analyzing $i..."
				/breakpad/tools/minidump_stackwalk \
					$i \
					/symbols \
					>  $i.txt \
					2> $i.error
			done
DOCKER_END
)"

}

analyze_dumps "$@"